package ucsc.cmps121.sobrietytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.Arrays;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.client.SessionConfiguration;

import com.uber.sdk.android.rides.RideRequestButton;

import com.uber.sdk.rides.client.ServerTokenSession;

import ucsc.cmps121.sobrietytest.profile.ProfileManager;

public class RideRequest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riderequest);

        findViewById(R.id.resultReturnButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(TestTimes.baseline){
            ProfileManager.getProfile(this).saveBaseline();
            String result = getTimeString("Color", TestTimes.colorTime);
            result += getTimeString("Math", TestTimes.mathTime);
            result += getTimeString("Maze", TestTimes.mazeTime);
            result += getTimeString("Sentence", TestTimes.sentenceTime);
            result += "Baseline set!";
            ((TextView)findViewById(R.id.resultsTextView)).setText(result);
        }else{
            SessionConfiguration config = new SessionConfiguration.Builder()
                    // mandatory
                    .setClientId("mpQDAHzVKrW7JkImhj3jDL8VyACE0nxq")
                    // required for enhanced button features
                    .setServerToken("4SrWY6V1KDtKK6YbTA4EEOsfF-MYgAUCGSfm0l5b")
                    // required for implicit grant authentication
                    .setRedirectUri("https://get.uber.com/go/?source=auth&next_url=https%3A%2F%2Fdeveloper.uber.com%2Fdashboard%2F")
                    // required scope for Ride Request Widget features
                    .setScopes(Arrays.asList(Scope.RIDE_WIDGETS))
                    .build();

            UberSdk.initialize(config);

            RideRequestButton requestButton = new RideRequestButton(getApplicationContext());

            RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout1);
            layout.addView(requestButton);

            RideParameters rideParams = new RideParameters.Builder()
                    // Optional product_id from /v1/products endpoint (e.g. UberX). If not provided, most cost-efficient product will be used
                    .setProductId("")
                    // Required for pickup estimates; lat (Double), lng (Double), nickname (String), formatted address (String) of pickup location
                    .setPickupToMyLocation()
                    .build();
// set parameters for the RideRequestButton instance
            requestButton.setRideParameters(rideParams);

            boolean drunk = ProfileManager.getProfile(this).isDrunk();
            String result = getTimeString("Color", TestTimes.colorTime);
            result += getTimeString("Math", TestTimes.mathTime);
            result += getTimeString("Maze", TestTimes.mazeTime);
            result += getTimeString("Sentence", TestTimes.sentenceTime);
            result += drunk ? "You might be too drunk to drive" : "You do not seem significantly impaired";
            ((TextView)findViewById(R.id.resultsTextView)).setText(result);
        }
    }

    private String getTimeString(String name, float time){
        return String.format("%s Time: %.02f sec\n", name, time);
    }
}
