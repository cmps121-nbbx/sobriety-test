package ucsc.cmps121.sobrietytest;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import java.util.Random;

public class React extends AppCompatActivity {
    public int trials= 20;
    public int score = 0;
    public int a = -1;  //holds desired color id
    public int b = -1;  //holds the users value
    public int c = -1;  // holds random number to represent text color
    public boolean start = false;
    public long result;
    private Chronometer Timer;
    Random rand = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_react);

        ActivityManager.runs = ActivityManager.runs +1;
        Button ReactStartBtn = (Button) findViewById(R.id.ReactStartBtn);
        Button GreenBtn = (Button) findViewById(R.id.GreenBtn);
        Button RedBtn = (Button) findViewById(R.id.RedBtn);
        Button BlueBtn = (Button) findViewById(R.id.BlueBtn);
        Button OrangeBtn = (Button) findViewById(R.id.OrangeBtn);
        Timer = (Chronometer) findViewById(R.id.Timer);
        ReactStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start == true){  // stops from running more than once
                    return;
                }
                Timer.start();
                change();
                start = true;

            }
        });
        GreenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b = 1;
                if(trials <= 0){
                    b = -5;
                    result = Timer.getBase();
                    Timer.stop();
                    change();
                }
                if(b == a){            //if correct
                    trials = trials -1;
                    change();
                    score++;
                }
                b = -1;  // reset value
            }
        });

        RedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b = 2;
                if(trials <= 0){
                    b = -5;
                    result = Timer.getBase();
                    Timer.stop();
                    change();
                }
                if(b == a){            //if correct
                    trials = trials -1;
                    change();
                    score++;

                }
                b = -1;  // reset value
            }
        });

        BlueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b = 3;
                if(trials <= 0){
                    b = -5;
                    result = Timer.getBase();
                    Timer.stop();
                    change();

                }
                if(b == a){            //if correct
                    trials = trials -1;
                    change();
                    score++;

                }
                b = -1;  // reset value
            }
        });

        OrangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b = 4;
                if(trials <= 0){
                    b = -5;
                    result = Timer.getBase();
                    Timer.stop();
                    change();
                }
                if(b == a){            //if correct
                    trials = trials -1;
                    change();
                    score++;

                }
                b = -1;  // reset value
            }
        });
    }

    public void change() {
        final TextView CommandTextView = (TextView) findViewById(R.id.CommandTextView);
        a = rand.nextInt(4) +1; // Green:1, Red:2, Blue:3, Orange:4
        c = rand.nextInt(4) +1; // same rules as above
        if(b == -5){                                            // this is where the game ends
            CommandTextView.setTextColor(Color.RED);
            CommandTextView.setText("Done");
            TestTimes.colorTime += (SystemClock.elapsedRealtime() - Timer.getBase()) / 1000f;
            if(ActivityManager.runs == 1){
                ActivityManager.runs = 0;
                Intent intentMath = new Intent(this, MathActivity.class);
                startActivity(intentMath);
            }else{
                Intent intentRepeat = new Intent(this, React.class);
                startActivity(intentRepeat);
            }
            finish();
            return;
        }
        if(a == 1){                                                          // Green case
            if(c == 1){ // make text green
                CommandTextView.setTextColor(Color.GREEN);
            }
            if(c ==2){  // ...red
                CommandTextView.setTextColor(Color.RED);
            }
            if(c == 3){  // ...blue
                CommandTextView.setTextColor(Color.BLUE);
            }
            if(c == 4){ // ...orange
                CommandTextView.setTextColor(Color.parseColor("#ffb23f"));
            }
            CommandTextView.setText("GREEN");
        }
        if(a == 2){                                                          // Red case
            if(c == 1){ // make text green
                CommandTextView.setTextColor(Color.GREEN);
            }
            if(c ==2){  // ...red
                CommandTextView.setTextColor(Color.RED);
            }
            if(c == 3){  // ...blue
                CommandTextView.setTextColor(Color.BLUE);
            }
            if(c == 4){ // ...orange
                CommandTextView.setTextColor(Color.parseColor("#ffb23f"));
            }
            CommandTextView.setText("RED");
        }
        if(a == 3){                                                           // Blue case
            if(c == 1){ // make text green
                CommandTextView.setTextColor(Color.GREEN);
            }
            if(c ==2){  // ...red
                CommandTextView.setTextColor(Color.RED);
            }
            if(c == 3){  // ...blue
                CommandTextView.setTextColor(Color.BLUE);
            }
            if(c == 4){ // ...orange
                CommandTextView.setTextColor(Color.parseColor("#ffb23f"));
            }
            CommandTextView.setText("BLUE");
        }
        if(a == 4){                                                          // case Orange
            if(c == 1){ // make text green
                CommandTextView.setTextColor(Color.GREEN);
            }
            if(c ==2){  // ...red
                CommandTextView.setTextColor(Color.RED);
            }
            if(c == 3){  // ...blue
                CommandTextView.setTextColor(Color.BLUE);
            }
            if(c == 4){ // ...orange
                CommandTextView.setTextColor(Color.parseColor("#ffb23f"));
            }
            CommandTextView.setText("Orange");
        }
    }
}
