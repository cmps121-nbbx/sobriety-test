package ucsc.cmps121.sobrietytest.profile;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

import ucsc.cmps121.sobrietytest.TestTimes;

/**
 * Created by Nathan on 11/29/2017.
 */

public class ProfileManager {
    public static String topKey = "ProfileManager";
    private Context context;
    private String profileKey;

    public static ProfileManager getProfile(Context context){
        ProfileManager manager = new ProfileManager(context, null);
        String name = manager.openPrefs(topKey).getString("lastProfile", "Default");
        return getProfile(context, name);
    }

    public void saveBaseline(){
        int count = getBaselineCount();
        TestRecord record = new TestRecord("baseline_"+count, TestTimes.mathTime, TestTimes.colorTime, TestTimes.mazeTime, TestTimes.sentenceTime);
        SharedPreferences.Editor editor = openPrefs(profileKey).edit();
        record.save(editor);
        editor.putInt("baselineCount",count+1).commit();
    }

    public boolean isDrunk(){
        TestRecord baseline = getProfileBaseline();
        float mathRatio = TestTimes.mathTime / baseline.mathTime;
        float colorRatio = TestTimes.colorTime / baseline.colorTime;
        float mazeRatio = TestTimes.mazeTime / baseline.mazeTime;
        float sentenceRatio = TestTimes.sentenceTime / baseline.sentenceTime;
        float max = 1.5f;
        return mathRatio > max || colorRatio > max || mazeRatio > max || sentenceRatio > max;
    }

    private static ProfileManager getProfile(Context context, String name){
        ProfileManager manager = new ProfileManager(context, name);
        manager.loadProfile();
        return manager;
    }

    protected ProfileManager(Context context, String name){
        this.context = context;
        if(name != null){
            this.profileKey = "profile_"+name;
        }
    }

    private void loadProfile(){
        if(context != null && profileKey != null){
            SharedPreferences pref = openPrefs(topKey);
            Set<String> profiles = pref.getStringSet("profiles", new HashSet<String>());
            if(!profiles.contains(profileKey)){
                profiles.add(profileKey);
                pref.edit().putStringSet("profiles", profiles).commit();
            }
        }
    }

    private int getBaselineCount(){
        return openPrefs(profileKey).getInt("baselineCount",0);
    }

    private TestRecord getProfileBaseline() {
        float math = 0f, color = 0f, maze = 0f, sentence = 0f;
        int count = getBaselineCount();
        if (count == 0) return getDefaultBaseline();
        SharedPreferences prefs = openPrefs(profileKey);
        for (int i = 0; i < count; i++) {
            math += prefs.getFloat("baseline_" + count + "_mathTime", 0);
            color += prefs.getFloat("baseline_" + count + "_colorTime", 0);
            maze += prefs.getFloat("baseline_" + count + "_mazeTime", 0);
            sentence += prefs.getFloat("baseline_" + count + "_sentenceTime", 0);
        }
        return new TestRecord("baselineAverage", math / count, color / count, maze / count, sentence / count);
    }

    protected TestRecord getDefaultBaseline(){
        return new TestRecord("baselineAverage", 15f, 30f, 15f, 60f);
    }

    private SharedPreferences openPrefs(String key){
        return context.getSharedPreferences(key, Context.MODE_PRIVATE);
    }

}
