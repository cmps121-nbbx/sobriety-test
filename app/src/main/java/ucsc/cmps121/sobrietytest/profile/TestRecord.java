package ucsc.cmps121.sobrietytest.profile;

import android.content.SharedPreferences;

/**
 * Created by Nathan on 12/4/2017.
 */
public class TestRecord{
    private String name;
    public float mathTime;
    public float colorTime;
    public float mazeTime;
    public float sentenceTime;

    public TestRecord(String name){
        this.name = name;
    }

    TestRecord(String name, float math, float color, float maze, float sentence){
        this.name = name;
        setTimes(math, color, maze, sentence);
    }

    void setTimes(float math, float color, float maze, float sentence){
        mathTime = math;
        colorTime = color;
        mazeTime = maze;
        sentenceTime = sentence;
    }

    void save(SharedPreferences.Editor editor){
        editor.putFloat(name+"_mathTime",mathTime);
        editor.putFloat(name+"_colorTime",colorTime);
        editor.putFloat(name+"_mazeTime",mazeTime);
        editor.putFloat(name+"_sentenceTime",sentenceTime);
    }
}