package ucsc.cmps121.sobrietytest.profile;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ucsc.cmps121.sobrietytest.R;

public class ProfileActivity extends AppCompatActivity {
    private static Set<String> profiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ((RadioButton)findViewById(R.id.defaultRadioButton)).setChecked(true);
        RadioGroup group = findViewById(R.id.profileRadioGroup);
        profiles = openPrefs().getStringSet("profiles", new HashSet<String>());
        String current = openPrefs().getString("lastProfile", "Default");
        for(String profile : profiles){
            RadioButton button = new RadioButton(this);
            button.setText(keyToName(profile));
            group.addView(button);
            if(current.equals(profile)) button.setChecked(true);
        }
        findViewById(R.id.createProfileButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewProfile();
            }
        });
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String name = ((RadioButton)findViewById(checkedId)).getText().toString();
                openPrefs().edit().putString("lastProfile", name).commit();
                loadPreferences();
            }
        });
        loadPreferences();
    }

    private void createRadioButton(String name){
        String key = nameToKey(name);
        openPrefs().edit().putString("lastProfile", name).commit();
        RadioGroup group = findViewById(R.id.profileRadioGroup);
        if(profiles.contains(key)){
            for(int i = 0; i < group.getChildCount(); i++){
                RadioButton test = (RadioButton) group.getChildAt(i);
                if(test.getText().equals(key)){
                    test.setChecked(true);
                }
            }
        }else{
            RadioButton button = new RadioButton(this);
            button.setText(name);
            group.addView(button);
            button.setChecked(true);
            ProfileManager.getProfile(this);
        }
    }

    private void createNewProfile(){
        final EditText enterName = new EditText(this);
        enterName.setSingleLine();
        new AlertDialog.Builder(this)
                .setTitle("Enter Profile Name")
                .setView(enterName)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String name = enterName.getText().toString();
                        createRadioButton(name);
                        loadPreferences();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private SharedPreferences openPrefs(){
        return getSharedPreferences(ProfileManager.topKey, MODE_PRIVATE);
    }

    @SuppressWarnings("unchecked")
    public void loadPreferences() {
        Map<String, ?> prefs = openPrefs().getAll();
        String combined = "ProfileManager:\n";
        for (String key : prefs.keySet()) {
            Object pref = prefs.get(key);
            String printVal = "";
            if (pref instanceof Boolean) {
                printVal =  key + " : " + (Boolean) pref;
            }
            if (pref instanceof Float) {
                printVal =  key + " : " + (Float) pref;
            }
            if (pref instanceof Integer) {
                printVal =  key + " : " + (Integer) pref;
            }
            if (pref instanceof Long) {
                printVal =  key + " : " + (Long) pref;
            }
            if (pref instanceof String) {
                printVal =  key + " : " + (String) pref;
            }
            if (pref instanceof Set<?>) {
                printVal =  key + " : " + (Set<String>) pref;
            }
            combined += printVal + "\n";
        }
        ((TextView)findViewById(R.id.debugTextView)).setText(combined);
    }

    private String keyToName(String key){
        return key.replace("profile_", "");
    }

    private String nameToKey(String name){
        return "profile_"+name;
    }

}
