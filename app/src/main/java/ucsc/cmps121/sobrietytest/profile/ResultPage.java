package ucsc.cmps121.sobrietytest.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ucsc.cmps121.sobrietytest.R;
import ucsc.cmps121.sobrietytest.TestTimes;

public class ResultPage extends AppCompatActivity {

    public boolean drunk = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_page);

        if(TestTimes.baseline == true){
            ProfileManager.getProfile(this).saveBaseline();
        }else{
            drunk = ProfileManager.getProfile(this).isDrunk();
        }
    }


}
