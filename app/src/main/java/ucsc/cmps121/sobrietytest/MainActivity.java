package ucsc.cmps121.sobrietytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.uber.sdk.android.core.UberButton;

public class MainActivity extends AppCompatActivity {

    public Button BaselineBtn;
    public Button DrunkBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BaselineBtn = (Button) findViewById(R.id.BaselineBtn);
        DrunkBtn = (Button) findViewById(R.id.DrunkBtn);

        BaselineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBaseline();
            }
        });

        DrunkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDrunk();
            }
        });
    }

    public void startBaseline(){
        Intent intentBase = new Intent(this, ReactInstruct.class);
        TestTimes.baseline = true;
        ActivityManager.react = true;
        startActivity(intentBase);
    }

    public void startDrunk(){
        Intent intentDrunk = new Intent(this, ReactInstruct.class);
        TestTimes.baseline = false;
        ActivityManager.react = true;
        startActivity(intentDrunk);
    }

}
