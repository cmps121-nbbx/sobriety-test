package ucsc.cmps121.sobrietytest.maze;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import ucsc.cmps121.sobrietytest.ActivityManager;
import ucsc.cmps121.sobrietytest.MazeActivity;
import ucsc.cmps121.sobrietytest.SentenceActivity;
import ucsc.cmps121.sobrietytest.TestTimes;

/**
 * Created by Nathan on 11/15/2017.
 */

public class MazeFragment extends Fragment {
    MazeGrid grid;
    MazeCell last;
    boolean win = false;
    private final static int MAZE_W = 8;
    private final static int MAZE_H = 12;
    private long startTime;

    public static MazeFragment newInstance(){
        return new MazeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MazeView mazeView =  new MazeView(getActivity());
        mazeView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onViewTouch((MazeView) v,event);
            }
        });
        mazeView.grid = grid = new MazeGrid(MAZE_W, MAZE_H);
        return mazeView;
    }

    public boolean onViewTouch(MazeView v, MotionEvent event){
        float x = event.getX() - v.getPaddingLeft();
        float y = event.getY() - v.getPaddingTop();
        MazeCell cell = v.getCellAt(x, y);
        boolean highlight = false;
        if(win){
            // if the end has been reached, a tap will reset to a new maze
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                v.grid = grid = new MazeGrid(MAZE_W, MAZE_H);
                v.win = win = false;
                last = null;
                v.timeElapsed = startTime = 0;
                v.invalidate();
            }
        }else if(cell != null){ // if a cell has been tapped, check to continue the path
            if(last == null){ // if the path has not begun, check if cell is the start
                if(cell.start){
                    last = cell;
                    cell.highlight = true;
                    startTime = SystemClock.elapsedRealtime();
                }
            }else{ // if the path is in progress, check if one away from last cell
                // set highlight to true if tapped cell adjacent to last and not obstructed by a wall
                if(last.x == cell.x){
                    if(last.y - cell.y == 1){
                        highlight = !cell.walls.south;
                    }else if(last.y - cell.y == -1){
                        highlight = !cell.walls.north;
                    }
                }else if(last.y == cell.y){
                    if(last.x - cell.x == 1){
                        highlight = !cell.walls.east;
                    }else if(last.x - cell.x == -1){
                        highlight = !cell.walls.west;
                    }
                }
            }
            if(highlight){
                if(!cell.highlight){
                    cell.highlight = true;
                    if(cell.end){
                        v.win = win = true;
                        v.timeElapsed = SystemClock.elapsedRealtime() - startTime;
                        float timeElapsed = v.getTimeElapsed();
                        // ends here
                        TestTimes.mazeTime = TestTimes.mazeTime + timeElapsed;
                        if(ActivityManager.runs == 1){  // would like it to do this
                            ActivityManager.runs = 0;
                            Intent intentSentence = new Intent(getContext(), SentenceActivity.class);
                            startActivity(intentSentence);
                        }else{
                            Intent intentRepeat = new Intent(getContext(), MazeActivity.class);
                            startActivity(intentRepeat);
                        }
                        getActivity().finish();
                    }
                    v.invalidate(v.getCellBounds(cell));
                }else{
                    last.highlight = false;
                    v.invalidate(v.getCellBounds(cell));
                }
                last = cell;
            }
        }
        return true;
    }

}
