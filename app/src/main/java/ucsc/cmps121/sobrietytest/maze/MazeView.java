package ucsc.cmps121.sobrietytest.maze;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Nathan on 11/15/2017.
 */

class MazeView extends View {
    public boolean win = false;
    public MazeGrid grid;
    public long timeElapsed;

    private Paints paints = new Paints();
    private int thickness;
    private int size;

    public MazeView(Context context) {
        super(context);
    }

    public MazeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onDraw(Canvas canvas){
        if(grid == null) return;
        setPadding(0, 0, 0, 0);
        int width = getWidth();
        int height = getHeight();
        size = Math.min(width / grid.width, height / grid.height);
        thickness = size / 10;
        int padW = (width % size) / 2;
        int padH = (height % size) / 2;
        setPadding(padW, padH, padW, padH);
        paints.text.setTextSize(size / 2);
        paintGrid(canvas);
    //    if(win) paintWin(canvas);
    }

    // overlays a victory message, giving the time elapsed
    public void paintWin(Canvas canvas){
        int xStart = getPaddingLeft() + size / 2;
        int yStart = getPaddingTop() + size * 2;
        int xEnd = getWidth() - getPaddingRight() - size / 2;
        int yEnd = getHeight() - getPaddingBottom() - size * 2;
        canvas.drawRect(xStart,yStart,xEnd,yEnd,paints.done);
        canvas.drawRect(xStart+thickness,yStart+thickness,xEnd-thickness,yEnd-thickness,paints.background);
        canvas.drawText("Maze Complete!", xStart + size / 2, yStart + size, paints.text);
        canvas.drawText(getTimeString(), xStart + size / 2, yStart + 3 * size / 2, paints.text);
    }

    private String getTimeString(){
        return String.format("Time Elapsed: %.2f sec",getTimeElapsed());
    }

    public float getTimeElapsed(){
        return timeElapsed / 1000f;
    }

    // paint each cell in the MazeGrid
    public void paintGrid(Canvas canvas){
        for(MazeCell[] row : grid.cells){
            for(MazeCell cell : row){
                paintCell(cell, canvas);
            }
        }
        for(MazeCell[] row : grid.cells){
            for(MazeCell cell : row){
                paintCellWalls(cell, canvas);
            }
        }
    }

    public MazeCell getCellAt(float xPos, float yPos){
        int x = (int)(xPos / size);
        int y = (int)(yPos / size);
        if(x < 0 || x >= grid.cells.length) return null;
        if(y < 0 || y >= grid.cells[0].length) return null;
        return grid.cells[x][y];
    }

    public Rect getCellBounds(MazeCell cell){
        if(cell == null) return new Rect(0,0,0,0);
        int xStart = size*cell.x + getPaddingLeft();
        int xEnd = xStart+size;
        int yStart = size*cell.y + getPaddingTop();
        int yEnd = yStart+size;
        return new Rect(xStart,yStart,xEnd,yEnd);
    }

    // fills a cell if it is on the current path, or if it is the start or end
    // draws a wall at each edge which has a wall enabled
    private void paintCell(MazeCell cell, Canvas canvas){
        Rect bounds = getCellBounds(cell);
        int xStart = bounds.left;
        int xEnd = bounds.right;
        int yStart = bounds.top;
        int yEnd = bounds.bottom;
        if(cell.highlight){
            canvas.drawRect(bounds,paints.path);
        }
        if(cell.start){
            canvas.drawRect(xStart,yStart,xStart+thickness,yStart-size/2,paints.wall);
            canvas.drawRect(xEnd-thickness,yStart,xEnd,yStart-size/2,paints.wall);
            canvas.drawRect(xStart,yStart,xEnd,yEnd,paints.start);
        }else if(cell.end){
            canvas.drawRect(xStart,yEnd,xStart+thickness,yEnd+size/2,paints.wall);
            canvas.drawRect(xEnd-thickness,yEnd,xEnd,yEnd+size/2,paints.wall);
            canvas.drawRect(xStart,yStart,xEnd,yEnd,win ? paints.done : paints.end);
        }
    }

    private void paintCellWalls(MazeCell cell, Canvas canvas){
        Rect bounds = getCellBounds(cell);
        int xStart = bounds.left;
        int xEnd = bounds.right;
        int yStart = bounds.top;
        int yEnd = bounds.bottom;
        if(cell.walls.north && !cell.start){
            canvas.drawRect(xStart,yStart,xEnd,yStart+thickness,paints.wall);
            canvas.drawCircle(xStart,yStart + thickness / 4,thickness * 2/3,paints.wall);
            canvas.drawCircle(xEnd,yStart + thickness / 4,thickness * 2/3,paints.wall);
        }
        if(cell.walls.south && !cell.end){
            canvas.drawRect(xStart,yEnd-thickness,xEnd,yEnd,paints.wall);
            canvas.drawCircle(xStart,yEnd - thickness / 4,thickness * 2/3,paints.wall);
            canvas.drawCircle(xEnd,yEnd - thickness / 4,thickness * 2/3,paints.wall);
        }
        if(cell.walls.west){
            canvas.drawRect(xStart,yStart,xStart+thickness,yEnd,paints.wall);
            canvas.drawCircle(xStart + thickness / 4,yStart,thickness * 2/3,paints.wall);
            canvas.drawCircle(xStart + thickness / 4,yEnd,thickness * 2/3,paints.wall);
            if(cell.x == 0) canvas.drawRect(xStart-thickness,yStart,xStart,yEnd,paints.wall);
        }
        if(cell.walls.east){
            canvas.drawRect(xEnd-thickness,yStart,xEnd,yEnd,paints.wall);
            canvas.drawCircle(xEnd - thickness / 4,yStart,thickness * 2/3,paints.wall);
            canvas.drawCircle(xEnd - thickness / 4,yEnd,thickness * 2/3,paints.wall);
            if(cell.x == grid.width - 1) canvas.drawRect(xEnd,yStart,xEnd+thickness,yEnd,paints.wall);
        }
    }

    private class Paints{
        public Paint background, wall, start, end, path, done, text;

        public Paints(){
            background = initPaint(Color.WHITE);
            wall = initPaint(Color.BLACK);
            start = initPaint(Color.BLUE);
            end = initPaint(Color.RED);
            path = initPaint(Color.CYAN);
            done = initPaint(Color.GREEN);
            text = initPaint(Color.BLACK);
        }

        private Paint initPaint(int color){
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(color);
            return paint;
        }
    }
}

