package ucsc.cmps121.sobrietytest.maze;

import android.graphics.Color;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by Nathan on 11/15/2017.
 */

public class MazeGrid{
    public MazeCell[][] cells;
    public int width;
    public int height;

    public MazeGrid(int w, int h){
        width = w;
        height = h;
        cells = new MazeCell[width][height];
        for(int x = 0; x < cells.length; x++) {
            for(int y = 0; y < cells[0].length; y++){
                cells[x][y] = new MazeCell(x,y);
            }
        }
        recursiveBacktrack();
    }

    public void recursiveBacktrack(){
        Stack<MazeCell> cellStack = new Stack<>();
        MazeCell current = cells[(int)(cells.length * Math.random())][0];
        current.start = true;
        current.visited = true;
        while(hasUnvisitedCell()){
            ArrayList<MazeCell> unvisited = current.getUnvisitedNeighbors(cells);
            if(unvisited.size() > 0){
                MazeCell next = unvisited.get((int)(unvisited.size() * Math.random()));
                cellStack.push(current);
                removeWall(current,next);
                current = next;
            }else if(!cellStack.isEmpty()){
                current = cellStack.pop();
            }
            current.visited = true;
        }
        cells[(int)(cells.length * Math.random())][cells[0].length-1].end = true;
    }

    public void removeWall(MazeCell first, MazeCell second){
        if(first.x == second.x){
            if(first.y > second.y){
                first.walls.north = second.walls.south = false;
            }else{
                first.walls.south = second.walls.north = false;
            }
        }else if(first.y == second.y){
            if(first.x > second.x){
                first.walls.west = second.walls.east = false;
            }else{
                first.walls.east = second.walls.west = false;
            }
        }else{
            throw new IllegalArgumentException("x1: "+first.x+", y1: "+first.y+"; x2: "+second.x+", y2: "+second.y);
        }
    }

    public boolean hasUnvisitedCell(){
        for(int x = 0; x < cells.length; x++) {
            for(int y = 0; y < cells[0].length; y++){
                if(!cells[x][y].visited) return true;
            }
        }
        return false;
    }

}
