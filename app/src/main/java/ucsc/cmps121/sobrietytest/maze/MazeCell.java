package ucsc.cmps121.sobrietytest.maze;

import java.util.ArrayList;

/**
 * Created by Nathan on 11/15/2017.
 */

public class MazeCell{
    public boolean start = false;
    public boolean end = false;
    public CellWalls walls = new CellWalls();
    public boolean visited = false;
    public boolean highlight = false;
    public int x;
    public int y;

    public MazeCell(int x, int y){
        this.x = x;
        this.y = y;
    }

    public ArrayList<MazeCell> getUnvisitedNeighbors(MazeCell[][] cells){
        ArrayList<MazeCell> neighbors = new ArrayList<>();
        if(x != 0 && !cells[x-1][y].visited) neighbors.add(cells[x-1][y]);
        if(y != 0 && !cells[x][y-1].visited) neighbors.add(cells[x][y-1]);
        if(x != cells.length-1 && !cells[x+1][y].visited) neighbors.add(cells[x+1][y]);
        if(y != cells[0].length-1 && !cells[x][y+1].visited) neighbors.add(cells[x][y+1]);
        return neighbors;
    }

    public class CellWalls{
        public boolean north = true;
        public boolean south = true;
        public boolean east = true;
        public boolean west = true;

        public CellWalls(){}
    }
}
