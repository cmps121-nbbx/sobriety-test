package ucsc.cmps121.sobrietytest;

import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import ucsc.cmps121.sobrietytest.maze.MazeFragment;

public class MazeActivity extends AppCompatActivity {
    public static final String MAZE_FLAG = "Maze";
    private LinearLayout rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityManager.runs = ActivityManager.runs +1;
        super.onCreate(savedInstanceState);
        rootView = new LinearLayout(this);
        setContentView(rootView);
        loadFragment();
    }

    private void loadFragment(){
        rootView.removeAllViews();
        FrameLayout mainLayout = new FrameLayout(this);
        rootView.addView(mainLayout, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        mainLayout.setId(10);

        MazeFragment mazeFragment = (MazeFragment) getSupportFragmentManager().findFragmentByTag(MAZE_FLAG);
        if(mazeFragment == null){
            mazeFragment = MazeFragment.newInstance();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(mainLayout.getId(), mazeFragment, MAZE_FLAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        setContentView(rootView);
        super.onConfigurationChanged(newConfig);
    }

}
