package ucsc.cmps121.sobrietytest;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import ucsc.cmps121.sobrietytest.profile.ResultPage;

public class SentenceActivity extends AppCompatActivity {

    public EditText input;
    public TextView problem;
    public Button push;
    public Chronometer timer;
    String inputString = new String();
    String problemString = new String();
    public String[] ranSentence = {"I am totally not drunk!", "I like dogs.", "I do not like cats",
            "I'm craving sushi.", "I hope I get an internship.", "Android is better than iOs!",
    "My cake is burning!", "There's a squirrel over there.", "My favorite chase is beer.", "I have so many deadlines coming up."};
    public String[] ranCompound = {"I can run, but I like to walk.", "I like beef, but I also like pork.",
    "I think I will buy the red car, or I will lease the blue one.", "I am counting my calories, yet I really want dessert.",
    "Everyone was busy, so I went to the movie alone.", "He did not want to go to the dentist, yet he went anyway.",
    "It was getting dark, and we were not there yet.", "The sky is clear; the stars are twinkling.",
    "They got there early, and they got really good seats.", "There was no ice cream in the freezer, nor did they have money to go to the store."};
    public int counter;
    public int counter2;
    public int finalTime;
    public long result;

    //Randomize the problem sentence so user doesn't become accustomed
    public void changeProblem(String[] sentenceStruc) {
        Long x = System.currentTimeMillis();
        Random rng = new Random(x);
        int z = rng.nextInt(10);
        String ran = sentenceStruc[z];
        //input string into problemString
        problem.setText(ran);
    }

    // Button transition that checks for matching strings(Will only transition if strings match)
    public void transition() {
        counter = 2;
        changeProblem(ranSentence);
        push = (Button)findViewById(R.id.button1);
        push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //checks strings and changes problem if check() == true
                if (check()) {
                    if(counter > 0) {
                        changeProblem(ranSentence);
                        input.setText("");
                        counter--;
                        if(counter == 0) {
                            counter2 = 2;
                        }
                    }
                    if(counter2 > 0) {
                        counter2--;
                        if(counter2 == 0) {
                            Toast.makeText(getApplicationContext(), "Done! Congrats!", Toast.LENGTH_LONG).show();
                            result = timer.getBase();
                            timer.stop();
                            TestTimes.sentenceTime += (SystemClock.elapsedRealtime() - timer.getBase()) / 1000f;
                            if(ActivityManager.runs == 1){
                                ActivityManager.runs = 0;
                                Intent intentResults = new Intent(SentenceActivity.this, RideRequest.class);
                                startActivity(intentResults);
                            }else{
                                Intent intentRepeat = new Intent(SentenceActivity.this, SentenceActivity.class);
                                startActivity(intentRepeat);
                            }
                            finish();
                        }else{
                            changeProblem(ranCompound);
                            input.setText("");
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Wrong! Try Again.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    //Checks if input string is equal to problem string
    public boolean check() {
        inputString = input.getText().toString();
        problemString = problem.getText().toString();
        if(inputString.equals(problemString)) {
            return inputString.equals(problemString);
        }
        else {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ActivityManager.runs = ActivityManager.runs + 1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentence);
        timer = (Chronometer)findViewById(R.id.Timer1);
        problem = (TextView)findViewById(R.id.textView1);
        input = (EditText)findViewById(R.id.editText1);
        timer.start();
        transition();
        }
    }

