package ucsc.cmps121.sobrietytest;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MathActivity extends AppCompatActivity {

    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    private Button six;
    private Button seven;
    private Button eight;
    private Button nine;
    private Button zero;
    private Button clear;
    private Button enter;
    private TextView question;
    private TextView lives;
    private TextView scores;
    private int counter;
    public int solution;
    private TextView enterNum;
    public TextView result;
    private int score;
    public int userInput;
    final String[] myEquations = {"+","-","*","/"};
    public int life = 3;
    public int finalTime;
    public Chronometer timer;
    public long resultTime;


    public void NumGenerator() {

        String rand = " ";
        String rand2 = " ";
        String sol = " ";
        Random rng = new Random();
        int randEq =(int)(Math.random()*4);
        int addSub = rng.nextInt(100)+1;
        int addSub2 = rng.nextInt(100)+1;
        int divMul = rng.nextInt(12)+1;
        int divMul2 = rng.nextInt(12)+1;



        if(randEq < 2){

            if(addSub > addSub2){
                rand = Integer.toString(addSub);
                rand2 = Integer.toString(addSub2);
                question.setText(rand + " " + myEquations[randEq] + " " + rand2);
                solution = Math(addSub,addSub2,randEq);
            }
            else{
                rand = Integer.toString(addSub);
                rand2 = Integer.toString(addSub2);
                question.setText(rand2 + " " + myEquations[randEq] + " " + rand);
                solution= Math(addSub2,addSub,randEq);

            }

            sol = Integer.toString(solution);
            // result.setText(sol);

        }

        else if(randEq == 2){

            rand = Integer.toString(divMul);
            rand2 = Integer.toString(divMul2);
            question.setText(rand + " " + myEquations[randEq] + " " + rand2);
            solution = Math(divMul,divMul2,randEq);


        }
        else {
            rand = Integer.toString(divMul*divMul2);
            rand2 = Integer.toString(divMul2);
            question.setText(rand + " " + myEquations[randEq] +" "+ rand2);
            solution = Math(divMul*divMul2, divMul2, randEq);
        }

        sol = Integer.toString(solution);
        // result.setText(sol);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math);

        setUIViews();
        NumGenerator();
        ActivityManager.runs = ActivityManager.runs +1;


//
//        lives.setText("Lives: " + life);
//        scores.setText("Score: " + score);

        timer = (Chronometer)findViewById(R.id.timer);
        timer.start();


        //get time
        finalTime = (int)(SystemClock.elapsedRealtime() - timer.getBase());


        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "0");
            }
        });

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "1");
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "2");
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "3");
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "4");
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "5");
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "6");
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "7");
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "8");
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterNum.setText(enterNum.getText().toString() + "9");
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enterNum.getText().length() > 0){
                    CharSequence name = enterNum.getText().toString();
                    enterNum.setText(name.subSequence(0, name.length()-1));
                }
                else{
                    enterNum.setText(null);
                }
            }
        });

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(enterNum.getText().toString().equals(""))) {
                    userInput = Integer.parseInt(enterNum.getText().toString());
                        Compare(userInput, solution);
                }
                else{
                    Toast.makeText(MathActivity.this, "Input a Number", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    private void setUIViews(){

        enter = (Button)findViewById(R.id.button_enter);
        clear = (Button)findViewById(R.id.button_clear);
        zero = (Button)findViewById(R.id.button0);
        one = (Button)findViewById(R.id.button1);
        two = (Button)findViewById(R.id.sentenceBtn);
        three = (Button)findViewById(R.id.button3);
        four = (Button)findViewById(R.id.button4);
        five = (Button)findViewById(R.id.button5);
        six = (Button)findViewById(R.id.button6);
        seven = (Button)findViewById(R.id.button7);
        eight = (Button)findViewById(R.id.button8);
        nine = (Button)findViewById(R.id.button9);
        enterNum = (TextView)findViewById(R.id.textView);
        question = (TextView)findViewById(R.id.question);
        //result = (TextView)findViewById(R.id.result);
//        lives = (TextView)findViewById(R.id.live);
//        scores = (TextView)findViewById(R.id.score);

    }

    public int Math(int x, int y, int eq){
        int result;

        if(eq == 0){
            result = x + y;
        }
        else if(eq == 1){
            result = x - y;
        }
        else if(eq == 2){
            result = x * y;
        }
        else{
            result = x / y;
        }

        return result;
    }


    public void Compare(int x, int y){

        // String scr = " ";
        //String health = " ";


            if( x != y ) {
              //  Toast.makeText(getApplicationContext(),"Incorrect!" ,Toast.LENGTH_SHORT).show();
                life--;
                enterNum.setText("");
            }
            else  {
               // Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_SHORT).show();
                score++;
                counter++;
                if (counter < 3){
                    NumGenerator();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
                    //get time here
                    resultTime = timer.getBase();
                    timer.stop();
                    finalTime = (int)(SystemClock.elapsedRealtime() - timer.getBase()); // at this point we shouldn't use this
                    TestTimes.mathTime += (SystemClock.elapsedRealtime() - timer.getBase()) / 1000f;

                }

                enterNum.setText("");
                if(score == 3){
                    if(ActivityManager.runs == 1){
                        ActivityManager.runs = 0;
                        Intent intentMaze = new Intent(this, MazeActivity.class);
                        startActivity(intentMaze);
                    }else{
                        Intent intentRepeat = new Intent(this, MathActivity.class);
                        startActivity(intentRepeat);
                    }
                    finish();
                }
            }






    }

}
