package ucsc.cmps121.sobrietytest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ReactInstruct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruct_react);

        Button ReadyBtn = (Button) findViewById(R.id.ReadyBtn);
        ReadyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reactIntent = new Intent(getApplicationContext(), React.class);
                startActivity(reactIntent);
                finish();
            }
        });
    }
}
